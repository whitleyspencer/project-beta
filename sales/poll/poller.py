import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            response = requests.get('http://inventory-api:8000/api/automobiles/')
            data = json.loads(response.content)
            for auto in data["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_href= auto["href"],
                    defaults = {
                        "import_href": auto["href"],
                        "vin": auto["vin"],
                        "model": auto["model"]["name"],
                        "manufacturer": auto["model"]["manufacturer"]["name"]
                    }
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
