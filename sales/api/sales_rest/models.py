from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href=models.CharField(max_length=250)
    vin=models.CharField(max_length=17, unique=True)
    model=models.CharField(max_length=100)
    manufacturer=models.CharField(max_length=100)


class Employee(models.Model):
    name=models.CharField(max_length=100)
    employee_number=models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_employee_detail", kwargs={"pk":self.id})


class Customer(models.Model):
    name=models.CharField(max_length=100)
    address=models.CharField(max_length=100)
    phone_number=models.CharField(max_length=15)

    def get_api_url(self):
        return reverse("api_customer_detail", kwargs={"pk":self.id})


class Sale(models.Model):
    automobile=models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )
    employee=models.ForeignKey(
        Employee,
        related_name="employees",
        on_delete=models.CASCADE
    )
    customer=models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete= models.CASCADE
    )
    price=models.CharField(max_length=15)

    def get_api_url(self):
        return reverse("api_sale_detail", kwargs={"pk":self.id})
