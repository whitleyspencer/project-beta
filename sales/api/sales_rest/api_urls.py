from django.urls import path
from .views import (
    api_list_employees,
    api_employee_detail,
    api_customer_detail,
    api_list_customers,
    api_list_sales,
    api_sale_detail,
)


urlpatterns = [
    path("employees/", api_list_employees, name="api_list_employees"),
    path("employees/<int:pk>/", api_employee_detail, name="api_employee_detail"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_sale_detail, name="api_sale_detail"),
]
