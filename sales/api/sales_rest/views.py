from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Employee, Customer, Sale, AutomobileVO
from .encoders import EmployeeEncoder, CustomerEncoder, SalesEncoder


@require_http_methods(["GET", "POST"])
def api_list_employees(request):
    if request.method=="GET":
        employees = Employee.objects.all()
        return JsonResponse({"employees": employees}, encoder=EmployeeEncoder, safe=False)
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(employee, EmployeeEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_employee_detail(request, pk):
    if request.method =="GET":
        try:
            employee = Employee.objects.get(id=pk)
            return JsonResponse(
                employee, EmployeeEncoder, safe=False
            )
        except(Employee.DoesNotExist):
            return JsonResponse({"No employee with ID": pk}, status=400)
    else:
        count, _ = Employee.objects.filter(id=pk).delete()
        return JsonResponse({"Delete Employee": count> 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method=="GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder, safe=False)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, CustomerEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_customer_detail(request, pk):
    if request.method =="GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer, CustomerEncoder, safe=False
            )
        except(Customer.DoesNotExist):
            return JsonResponse({"No customer with ID": pk}, status=400)
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"delete customer": count>0})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method =="GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, SalesEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin = automobile_vin)
            content["automobile"] = automobile
            if Sale.objects.filter(automobile=automobile).exists():
                return JsonResponse({"message": "Sale already exists"}, status=400)
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Invalid automobile vin"}, status=400)

        try:
            employee_id = content["employee"]
            employee = Employee.objects.get(id = employee_id)
            content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse({"message": "Invalid Employee id"}, status=400)

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id = customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid Customer id"}, status=400)

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, SalesEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_sale_detail(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale, SalesEncoder, safe=False
        )
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"delete sale": count>0})
