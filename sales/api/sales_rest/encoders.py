from .models import Employee, Customer, AutomobileVO, Sale
from common.json import ModelEncoder


class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = ["name", "employee_number", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "model",
        "manufacturer"
    ]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "customer",
        "employee",
        "price",
    ]

    encoders= {
        "customer": CustomerEncoder(),
        "employee": EmployeeEncoder(),
        "automobile": AutomobileVOEncoder()
    }
