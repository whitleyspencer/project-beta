import { useEffect, useState } from 'react'


export default function NewSaleForm() {
  const [formData, setFormData] = useState({
    "automobile": "",
    "customer": "",
    "employee": "",
    "price": ""
  })

  const handleFormChange = (event) => {
    const value = event.target.value
    const inputName = event.target.name
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  const [automobiles, setAutomobiles] = useState([])
  const getAutomobileData = async ()=> {
    const response = await fetch("http://localhost:8100/api/automobiles/")
    if (response.ok) {
      const data = await response.json()
      setAutomobiles(data.autos)
    }
  }
  useEffect(() => {getAutomobileData()}, [])

  const[sales, setSales] = useState([])
  const getSalesData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/")
    if (response.ok) {
      const data = await response.json()
      setSales(data.sales)
    }
  }
  useEffect(()=> {getSalesData()}, [])

  const [customers, setCustomers] = useState([])
  const getCustomerData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/")
    if (response.ok) {
      const data = await response.json()
      setCustomers(data.customers)
    }
  }
  useEffect(()=> {getCustomerData()}, [])

  const [employees, setEmployees] = useState([])
  const getEmployeeData = async () => {
    const response = await fetch("http://localhost:8090/api/employees/")
    if (response.ok) {
      const data = await response.json()
      setEmployees(data.employees)
    }
  }
  useEffect(()=> {getEmployeeData()}, [])

  const soldVins = []
  for (let sale of sales) {
    soldVins.push(sale.automobile.vin)
  }
  const currentInventory = []
  for (let auto of automobiles) {
    if (!(soldVins.includes(auto.vin))) {
      currentInventory.push(auto)
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const url = "http://localhost:8090/api/sales/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers : {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      setFormData({
        "automobile": "",
        "customer": "",
        "employee": "",
        "price": ""
      })
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6 shadow p-4 mt-4">
        <h2>New Sale</h2>
        <form id="create-customer-form" onSubmit={handleSubmit}>
          <div className="mb-3">
            <select
              value={formData.automobile}
              onChange={handleFormChange}
              required
              name="automobile"
              id="automobile"
              className="form-select">
                <option value="">Automobile</option>
                {currentInventory.map(availableAuto => {
                  return (
                    <option key={availableAuto.href} value={availableAuto.vin}>
                      VIN: {availableAuto.vin} ({availableAuto.model.manufacturer.name} {availableAuto.model.name})
                    </option>
                  )
                })}
            </select>
          </div>

          <div className="mb-3">
            <select
              value={formData.employee}
              onChange={handleFormChange}
              required
              name="employee"
              id="employee"
              className="form-select">
                <option value="">Employee</option>
                {employees.map(employee => {
                  return (
                    <option key={employee.href} value={employee.id}>
                      {employee.name}
                    </option>
                  )
                })}
            </select>
          </div>

          <div className="mb-3">
            <select
              value={formData.customer}
              onChange={handleFormChange}
              required
              name="customer"
              id="customer"
              className="form-select">
                <option value="">Customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.href} value={customer.id}>
                      {customer.name}
                    </option>
                  )
              })}
            </select>
          </div>

          <div className="form-floating mb-3">
              <input
                onChange ={handleFormChange}
                value ={formData.price}
                placeholder="price"
                required
                type="text"
                id="price"
                name="price"
                className="form-control" />
              <label htmlFor="price">Price</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  )
}
