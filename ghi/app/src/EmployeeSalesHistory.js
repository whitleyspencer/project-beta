import React, { useEffect, useState } from 'react'


function EmployeeSales(props) {
  const employeeSales = []
  for (let sale of props.sales) {
    if (sale.employee.employee_number == props.selectKey) {
      employeeSales.push(sale)
    }
  }

  if (employeeSales.length === 0) {
    return (
        <h5 className="lead text-muted mt-3">No sales History</h5>
    )
  } else {
    return (
      <>
        {
          employeeSales.map(sale => {
            return (
              <tr key={sale.href}>
                <td>{ sale.employee.name }</td>
                <td>{ sale.employee.employee_number }</td>
                <td>{ sale.customer.name }</td>
                <td>{ sale.automobile.vin }</td>
                <td>{ sale.price }</td>
              </tr>
            )
          })
        }
      </>
    )
  }
}

export default function SalesHistoryPage() {
  const [employees, setEmployees] = useState([])
  const getEmployeeData = async () => {
    const response = await fetch("http://localhost:8090/api/employees/")
    if (response.ok) {
        const data = await response.json()
        setEmployees(data.employees)
    }
  }
  useEffect(()=> {getEmployeeData()}, [])

  const [sales, setSales] = useState([])
  const getSalesData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/")
    if (response.ok) {
        const data = await response.json()
        setSales(data.sales)
    }
  }
  useEffect(()=> {getSalesData()}, [])

  const [selectKey, setSelectKey] = useState("")
  const handleKeyChange = (event) => {
    setSelectKey(event.target.value)
  }

  return (
    <div className="container my-3">
      <h2>Sales History</h2>
      <select
        onChange={handleKeyChange}
        value={selectKey}
        id="employee"
        name="employee"
        className="form-select">
        <option value="">Sales Associate</option>
          {employees.map(employee=> {
            return (
              <option value={employee.employee_number} key={employee.href}>{employee.name}</option>
            )
          })}
      </select>
      <table className="table table-striped mt-2">
        <thead>
          <tr>
            <th>Sales Associate</th>
            <th>Employee Number</th>
            <th>Purchaser</th>
            <th>Automobile Vin</th>
            <th>Purchase Price</th>
          </tr>
        </thead>
          <tbody>
            <EmployeeSales sales={sales} selectKey={selectKey} />
          </tbody>
      </table>
    </div>
  )
}
