import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewSaleForm from './NewSaleForm';
import SalesList from './SalesList';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import NewCustomer from './NewCustomer';
import NewSalesAssociate from './NewSalesAssociate';
import SalesHistoryPage from './EmployeeSalesHistory';
import ManufactutersList from './ManufacturersList';
import AppointmentHistory from './AppointmentHistory';
import NewManufacturer from './NewManufacturer';
import ModelsList from './ModelsList';
import NewModel from './NewModel';
import AutomobileList from "./AutomobileList";
import NewAutomobile from './NewAutomobile';


function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="appointments" >
            <Route path="" element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
            <Route path="technicians" element={<TechnicianForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="new" element={<NewSaleForm />} />
            <Route path="customers/new" element={<NewCustomer />} />
            <Route path="employees/new" element={<NewSalesAssociate />} />
            <Route path="history" element={<SalesHistoryPage />}/>
          </Route>
          <Route path="inventory">
            <Route path="manufacturers" element={<ManufactutersList />} />
            <Route path="manufacturers/new" element={<NewManufacturer />} />
            <Route path="models" element={<ModelsList />} />
            <Route path="models/new" element={<NewModel/>} />
            <Route path="automobiles" element={<AutomobileList />} />
            <Route path="automobiles/new" element={<NewAutomobile />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
