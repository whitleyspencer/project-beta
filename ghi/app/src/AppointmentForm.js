import React, { useState, useEffect } from "react"

function AppointmentForm() {
  const [formClass, setFormClass] = useState("")
  const [successClass, setSuccessClass] = useState(
    "alert alert-success d-none"
  )
  const [name, setName] = useState("")
  const [service, setService] = useState("")
  const [vin, setVin] = useState("")
  const [date, setDate] = useState("")
  const [technician, setTechnician] = useState("")

  const [technicians, setTechnicians] = useState([])
  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/"
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleNameChange = (event) => {
    setName(event.target.value)
  }
  const handleServiceChange = (event) => {
    setService(event.target.value)
  }
  const handleVinChange = (event) => {
    setVin(event.target.value)
  }
  const handleDateChange = (event) => {
    setDate(event.target.value)
  }
  const handleTechChange = (event) => {
    setTechnician(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}
    data.customer_name = name
    data.date = date
    data.service = service
    data.vin = vin
    data.technician_id = technician

    const appointmentUrl = "http://localhost:8080/api/appointments/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    }
    const response = await fetch(appointmentUrl, fetchConfig)
    if (response.ok) {
      setFormClass(data)
      setSuccessClass("alert alert-success")
    }
  }

  return (
    <div className="row">
      <div className="offset-0 col-">
        <div className="shadow p-2 mt-2">
          <h1>Schedule Your Appointment</h1>
          <form
            className={formClass}
            onSubmit={handleSubmit}
            id="create-appointment-form"
          >
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Customer Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Customer Name</label>
            </div>

            <div className="form mb-3">
              <label htmlFor="service" className="form-label">
                Issue
              </label>
              <textarea
                placeholder="What brings you in today?"
                onChange={handleServiceChange}
                className="form-control"
                required
                name="service"
                id="service"
                rows="3"
              ></textarea>
            </div>

            <div className="form-floating mb-3">
              <input
                value={vin}
                onChange={handleVinChange}
                placeholder="Vehicle VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={date}
                onChange={handleDateChange}
                placeholder="mm/dd/yyyy"
                required
                type="datetime-local"
                name="datetime-local"
                id="datetime-local"
                className="form-control"
              />
              <label htmlFor="datetime-local">Date of appointment</label>
            </div>

            <div className="mb-3">
              <select
                value={technician}
                onChange={handleTechChange}
                required
                name="technician"
                id="technician"
                className="form-select"
              >
                <option value="">Technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {" "}
                      {technician.name}{" "}
                    </option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-outline-success">Submit</button>
          </form>
          <div className={successClass} role="alert">
            {" "}
            Great success!{" "}
          </div>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm
