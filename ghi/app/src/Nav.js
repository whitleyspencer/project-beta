import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success shadow-sm">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </button>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="sales">All sales</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="sales/new">New Sale</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="sales/customers/new">New Customer</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="sales/history">Sales History</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </button>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="appointments/new">New Appointment</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="appointments">Current Appointments</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="appointments/history">Appointment History</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/manufacturers">Manufacturers</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/manufacturers/new">New Manufacturer</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/models">Models</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/models/new">New Model</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/automobiles">Automobiles</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="inventory/automobiles/new">New Automobile</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn text-white dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Employees
              </button>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="sales/employees/new">New Sales Associate</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" aria-current="page" to="appointments/technicians">New Technician</NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
