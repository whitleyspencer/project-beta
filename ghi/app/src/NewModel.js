import { useState, useEffect } from 'react'

export default function NewModel() {
    const [formData, setFormData] = useState({
        name: "",
        manufacturer_id: "",
        picture_url: ""
    })

    const [manufacturers, setManufacturers] = useState([])
    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(()=> {getData()}, [])

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                name: "",
                manufacturer_id: "",
                picture_url: ""
            })
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6 shadow p-4 mt-4">
                <h2>New Model</h2>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="mb-3">
                        <select
                            value={formData.manufacturer_id}
                            onChange={handleFormChange}
                            required
                            name="manufacturer_id"
                            id="manufacturer_id"
                            className="form-select">
                                <option value="">Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.href} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange ={handleFormChange}
                            value ={formData.name}
                            placeholder="model name"
                            required
                            type="text"
                            id="name"
                            name="name"
                            className="form-control"/>
                        <label htmlFor="address">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange ={handleFormChange}
                            value ={formData.picture_url}
                            placeholder="picture url"
                            required
                            type="text"
                            id="picture_url"
                            name="picture_url"
                            className="form-control" />
                        <label htmlFor="address">Picture URL</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    )
}
