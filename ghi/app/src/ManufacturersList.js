import React, { useState, useEffect } from "react";


export default function ManufactutersList() {
    const [manufacturers, setManufacturers] = useState([])
    const getManufacturersData = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(()=> {getManufacturersData()}, [])

    return (
        <div className="container mt-3">
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturers</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={ manufacturer.href }>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
