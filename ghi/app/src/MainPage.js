import { NavLink } from 'react-router-dom';

function MainPage() {
  return (
    <>
      <img className="w-100 card-img opacity-75" src="https://images.pexels.com/photos/681347/pexels-photo-681347.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" alt="of interstate" />
      <div className="px-4 py-5 mt-5 mx-5 text-center card-img-overlay">
        <div className="card text-center">
          <div className="card-body">
            <h1 className="display-5 fw-bold">CarCar</h1>
            <div className="col-lg-6 mx-auto">
              <p className="lead mb-4">
                The premiere solution for automobile dealership management!
              </p>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="card w-25 mt-5 mx-3 col">
            <div className="card-body">
              <h5 className="card-title fw-bold">Inventory</h5>
              <p className="card-text">View or register new automobiles, manufacturers, and vehicle models.</p>
              <img className="card-img-top" src="https://images.pexels.com/photos/7144214/pexels-photo-7144214.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" alt="of car sales person" />
              <NavLink className="btn btn-primary mt-2" to="inventory/automobiles">Inventory</NavLink>
            </div>
          </div>

          <div className="card w-25 mt-5 mx-3 col">
            <div className="card-body">
              <h5 className="card-title fw-bold">Sales</h5>
              <p className="card-text">View automobile sales, register new sales, or manage potential customers.</p>
              <img className="card-img-top" src="https://images.pexels.com/photos/7144199/pexels-photo-7144199.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" alt="of car sales person" />
              <NavLink className="btn btn-primary mt-2" to="sales">Sales Center</NavLink>
            </div>
          </div>

          <div className="card w-25 mt-5 mx-3 col">
            <div className="card-body">
              <h5 className="card-title fw-bold">Service</h5>
              <p className="card-text">View current service schedule, service history, or manage appointments.</p>
              <img className="card-img-top" src="https://images.pexels.com/photos/2244746/pexels-photo-2244746.jpeg?auto=compress&cs=tinysrgb&w=800" alt="of car being serviced" />
              <NavLink className="btn btn-primary mt-2" to="appointments">Service Center</NavLink>
            </div>
          </div>
        </div>

      </div>
    </>
  );
}

export default MainPage;
