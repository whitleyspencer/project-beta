import React, { useState, useEffect } from "react";


export default function ModelsList() {
    const [models, setModels] = useState([])
    const getModelsData = async () => {
        const response = await fetch("http://localhost:8100/api/models/")
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }
    useEffect(()=> {getModelsData()}, [])

    return (
        <div className="container mt-3">
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={ model.href }>
                                <td>{ model.manufacturer.name }</td>
                                <td>{ model.name }</td>
                                <td>
                                    <img
                                        src={ model.picture_url }
                                        alt={ `${ model.manufacturer.name } ${model.name}` }
                                        className="img-thumbnail w-25 mx-auto d-block"/>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
