import { useState } from 'react'


export default function NewCustomer() {
  const [formData, setFormData] = useState({
    name: "",
    address: "",
    phone_number: "",
  })

  const handleFormChange = (event) => {
    const value = event.target.value
    const inputName = event.target.name
    setFormData({
      ...formData,
      [inputName]: value
    })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const url = "http://localhost:8090/api/customers/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers : {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      setFormData({
        name: "",
        address: "",
        phone_number: "",
      })
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6 shadow p-4 mt-4">
        <h2>New Customer</h2>
        <form onSubmit={handleSubmit} id="create-customer-form">
          <div className="form-floating mb-3">
            <input
              onChange ={handleFormChange}
              value ={formData.name}
              placeholder="name"
              required
              type="text"
              id="name"
              name="name"
              className="form-control" />
            <label htmlFor="name">Customer Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange ={handleFormChange}
              value ={formData.address}
              placeholder="address"
              required
              type="text"
              id="address"
              name="address"
              className="form-control" />
            <label htmlFor="address">Address</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange ={handleFormChange}
              value ={formData.phone_number}
              placeholder="phone number"
              required
              type="text"
              id="phone_number"
              name="phone_number"
              className="form-control" />
            <label htmlFor="phone_number">Phone Number</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  )
}
