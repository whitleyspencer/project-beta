import { useState, useEffect } from 'react'

export default function SalesList() {
    const [sales, setSales] = useState([])
    const getSalesData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }}
    useEffect(()=> {getSalesData()}, [])

    return (
        <div className="container my-3">
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Associate</th>
                        <th>Employee Number</th>
                        <th>Purchaser</th>
                        <th>Automobile Vin</th>
                        <th>Purchase Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.href}>
                                <td>{ sale.employee.name }</td>
                                <td>{ sale.employee.employee_number }</td>
                                <td>{ sale.customer.name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ sale.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
