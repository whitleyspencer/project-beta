from common.json import ModelEncoder
from .models import Appointment, Technician, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin"
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "service",
        "status",
        "technician",
    ]
    encoders = {"technician": TechnicianEncoder()}

    def get_extra_data(self, o):
        return {
            "technician": o.technician.name
        }
