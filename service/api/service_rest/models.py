from django.db import models
from django.urls import reverse


class Technician(models.Model):
    employee_number = models.PositiveIntegerField(unique=True)
    name = models.CharField(max_length=50)

    def get_api_url(self):
        return reverse("api_list_technician", kwargs={"id": self.pk})


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=25)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=150)
    date = models.DateTimeField()
    service = models.TextField(null=True)
    status = models.CharField(max_length=30, default="INCOMPLETE")
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT
    )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
