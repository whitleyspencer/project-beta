# CarCar

Team:

- Person 1 - Joe Biedermann (Service microservice)
- Person 2 - Whitley Spencer (Sales microservice)

## Design

We will be using Bootstrap.

## Service microservice

I started by creating my models which were my Appointments and Technician. Appointments is so the user can create and appointment by entering their Vin, name, date and time, and service they require. It also keeps track of the technician assigned to their service and status of said service. Completed services are stored after completion or deletion of their appointment. Vin numbers are shared between inventory and service allowing a VIP status to be applied for vins that were once in our database prompting the concierge to provide our "VIP treatment."

The Technician is used to create a service employee with a name and a number. You're then able to assign them to the services that are created.



## Sales microservice

The Sale microservice is responsible for managing data related to car sales. I created models to store data for the four major components of a car sale- the employee, the customer, the sale itself, and the car being sold.
The Employee model contans fields for name and employee number. The Customer model contains fields for name, address, and phone number. The AutomobileVO model contains copies of automobile data requested from the Inventory microservice, including VIN number, manufacturer name, and model name. The AutomobileVO objects are updated through repeated polling requests to the Inventory microservice. Lastly, The Sale model contains foreignKeys for customer, employee, and automobile, and a field for price.
